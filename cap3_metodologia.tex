\chapter{Metodologia}\label{chap-methods}
    
   
  	\section{Modelo de Anéis Ativos Modificado}
  	
  	Neste capítulo introduzimos o modelo que utilizamos no presente trabalho. Como discutido anteriormente, o modelo de anéis composto de partículas passivas conectadas por molas foi introduzido por Boromand et al 2018 e modificado no trabalho anterior em Teixeira et al 2021, onde partículas ativas com autoalinhamento formam o anel e uma energia de curvatura é introduzida para fornecer rigidez ao invés de uma energia de conservação de área. No trabalho presente, usamos energia de conservação de área ao invés da energia de curvatura devido ao fato de garantir um valor de área alvo em torno de um valor de equilíbrio $A_{0}$ para o anel ativo, uma característica observada em sistemas celulares, e utilizada em modelos tipo Voronoi-Vertex onde parâmetros adimensionais de controle são úteis na descrição do sistema. As artículas ativas com autoalinhamento que compõem o anel têm capacidade de alinhar a direção da polaridade com a da velocidade, que foram inicialmente introduzidas por \emph{Szabó et al 2006} e \emph{Henkes et al 2011}, como discutido anteriormente. Tal capacidade propicia o surgimento de estados coletivos translacionais, rotacionais e alternancia entre ambos estados como visto discutido no capítulo anterior e discutido em Teixeira et al 2021. Neste trabalho, visando o estudo da segregação celular por adesão e tensão interfacial diferenciada, utilizaremos uma versão modificada do anel ativo anteriormente apresentado, porém alterando a forma como a polaridade aparece. A polaridade em \emph{Teixeira et al 2021} é uma propriedade das partículas individuais que formam o anel, que alinham sua polaridade na direção da velocidade após interações com vizinhos, gerando assim estados rotacionais e translacionais. No presente trabalho, buscando evitar o surgimento de estados rotacionais internos nos anéis, alteramos a forma como a polaridade é expressa, bem como o mecanismo de alinhamento. Definimos nesse trabalho a polaridade como uma propriedade global do anel (célula), conforme observado na dinâmica celular. Além disso, propomos uma modificação no mecanismo de alinhamento para que o anel ativo se comporte como uma partícula ativa com autoalinhamento, permitindo o surgimento de estados de coletividade translacionais entre anéis. A polaridade tende a se alinhar com a velocidade do anel (centro de massa) após interações com outros anéis, não sendo um mecanismo a nível de partícula individual. Portanto, modelamos um sistema celular em $2D$ a partir de $N$ anéis ativos constituídos de $n$ partículas ativas com autoalinhamento \cite{szabo,sarkar2021minimal}. Além disso, desprezamos os efeitos inerciais supondo um regime de baixo número de Reynolds~\cite{purcell1977life,bechinger2016active}, que é padrão em sistemas biológicos celulares. As equações superamortecidas~\cite{szabo} que governam a dinâmica de cada partícula são

\begin{eqnarray}
\label{1}
  \frac{\mathrm{d}\vec{r}_{i,j}}{\mathrm{d} t}   &=& v_{0}\,\hat{n}_{j} -\mu\vec{\nabla}_{i,j} E,  \\
\label{2}
\frac{\mathrm{d}\hat{n}_{j}}{\mathrm{d} t}  &=& J(\hat{n}_{j}\times \hat{v}^{j}_{cm})\times \hat{n}_{j}+ \sqrt{2\,D_{R}}\,\vec{\xi}_{j}\times \hat{n}_{j},
\end{eqnarray}
onde $\vec{r}_{i,j}(t)$ denota a $i$-ésima posição da partícula pertencente ao $j$-ésimo anel no tempo $t$, $\mu$ sua mobilidade e $v_ {0} = \mu F_{0}$ a magnitude da velocidade de autopropulsão ao longo da orientação, $\hat{n}_{j}(t)$. $F_{0}$ é chamada de força ativa.  A direção da força ativa descrita pelo vetor unitário $\hat{n}_{j}(t)$, relaxa para a direção da velocidade do centro de massa $\hat{v}^{j}_{cm} = \frac {\vec{v}^{j}_{cm}}{|\vec{v}^{j}_{cm}|}$ dentro de um tempo característico $\tau = J^{-1}$, enquanto também experimenta ruído branco gaussiano angular $\vec{\xi_{j}}(t) = \xi_{j}(t)\hat{e}_{z}$ de correlação $\left \langle \xi_{j} (t_{1})\xi_{k}(t_{2}) \right \rangle = \delta_{jk}\delta (t_{1}-t_{2})$ independentemente para partículas de anéis diferentes a cada vez passo de tempo.  $D_{R}$ é o coeficiente de difusão rotacional e define uma escala de tempo de persistência típica, $\tau_R= 1/D_R$, para mudanças devido ao ruído angular. 

A energia $E$ elástica relacionada as interações internas e externas é composta por quatro termos
\begin{equation}
\label{3}
    E = E_{P} + E_{A} + E_{c} + E_{\Lambda}.
\end{equation}
O primeiro termo representa um termo de contratilidade, onde as partículas adjacentes são conectadas através de molas lineares, assim
\begin{equation}
\label{4}
    E_{P} = \frac{\epsilon_{P}}{2}\sum_{j=0}^{N} \sum_{i=0}^{n} \left ( \frac{|\vec{l}_{i,j}|}{l_{0}} - 1 \right )^{2},
\end{equation}
onde $\vec{l}_{i,j} = \vec{r}_{i,j} - \vec{r}_{i-1,j}$ é o vetor que conecta partículas consecutivas no anel (veja a Fig.~\ref{rings_scheme}), $e_{l}$ é a energia elástica da mola que controla as flutuações do perímetro e $l_{0}$ é a distância de equilíbrio no anel. Além disso, para levar em conta a resistência celular em comprimir-se, utilizamos um potencial de área
 
 \begin{eqnarray}
E_{A} = \frac{\epsilon_{A}}{2}\sum_{j=0}^{N} \sum_{i=0}^{n}\left (\frac{A_{j}}{A_{0}}-1 \right )^{2},
 \end{eqnarray}
onde $\epsilon_{A}$ é a energia elástica relacionada ao controle da área, $A$ é a área do anel (não levando em conta a área das partículas) e $A_{0}$ é a área de equilíbrio.
 Modelamos a interação de exclusão de volume entre as partículas dentro (não vizinhas) e fora do anel usando um potencial harmônico,
 \begin{eqnarray}
 %U_{rep} =\begin{matrix}
%\frac{K_{rep}(r_{ij}-r_{eq})^{2}}{2}, & r_{ij} < r_{eq},
%\end{matrix}
 E_{c} = \left\{\begin{matrix}
 \frac{\epsilon_{c}}{2}\left (\frac{r_{ik}}{\sigma}-1\right )^{2} & r_{ik} \leq \sigma \\ 
 0& r_{ik} > \sigma
\end{matrix}\right.
 \end{eqnarray}
 onde $r_{ik}$ é a distância entre as partículas $i$ e $k$, $\sigma$ é a distância de equilíbrio (que é definida aqui como tamanho efetivo de partícula). 
 
      \begin{figure}[!h]
        \centering
        %\includegraphics[width=7.6cm, height=6.5cm]{ring.pdf}
        %\includegraphics[width=8.27cm, height=5.27cm]{ring_bing.png}
        \includegraphics[width=0.8\columnwidth]{configuration_sistema.png}
        \caption{(a) Configuração inicial do sistema. (b) Tensões interfaciais diferenciais entre anéis ativos.}
\label{Ring scheme}
\end{figure}
 
 Para controlar a tensão interfacial entre diferentes anéis, utilizamos um potencial de tensão linear, apresentado inicialmente por \emph{boromand et al 2018}, que define a tensão que uma determinada partícula de um anel está sujeita. A tensão dependerá se a partícula está em contato com outra pertencente a um anel diferente e o tipo de ambos anéis.  Neste trabalho, haverá dois tipos de anéis: vermelhos, definidos com índice 1 e verdes com índice 2. Assim, temos
 \begin{equation}
       E_{\Lambda} = \Lambda_{\alpha \beta}\sum_{j=0}^{N} \sum_{i=0}^{n} |\vec{l}_{i,j}|,
 \end{equation}
onde $\Lambda_{11}$ é a tensão entre as partículas pertencentes aos anéis vermelhos e $\Lambda_{22}$ aos anéis verdes. A tensão entre anéis de diferentes tipos é definida como $\Lambda_{12} = \Lambda_{21}$. Na Fig. \ref{rings_scheme} podemos observar uma representação esquemática do sistema de anéis ativos de dois tipos, como as tensões envolvidas, distâncias de equilíbrio, área, polaridade do anel e velocidade das partículas.

\subsection{Configuração do Sistema}

O sistema consiste em um agregado de dois tipos de anéis ativos, os quais são misturados (ver Fig. \ref{system_setup}). A proporção utilizada consiste em 4 anéis do tipo 2 para cada um do tipo 1. 
Além disso, para formar o agregado, usamos uma força externa que tende a puxar os anéis em direção ao centro de massa sempre que eles se movem a uma distância $|\vec{r}_{i,CM}| > R_{0}$, onde $|\vec{r}_{i,CM}|$ é a distância do i-ésimo anel ao centro de massa do sistema. Assim, definimos a força relacionada à pressão externa devido à influência do meio no sistema celular como
\begin{eqnarray}    
    E_{pressure} =  \left\{\begin{matrix}
 \epsilon_{pressure}\left (\frac{|\vec{r}_{i,CM}|}{R_{0}} - 1 \right ) & |\vec{r}_{i,CM}| \geq  R_{0}, \\ 
 0& |\vec{r}_{i,CM}| < R_{0},
\end{matrix}\right. 
\end{eqnarray}
onde $\hat{r}_{i,CM}$ é o vetor unitário que conecta o centro de um determinado anel $i$ ao centro de massa, $E_{pressure}$ é a energia relacionado a pressão externa que um determinado anel está submetido e $R_{0}$ é a distância na qual o anel começa a ser atraído em direção ao centro de massa, sendo $\pi R_{0}^{2} = NA_{0}$.

\subsection{Parâmetros de Ordem}

Para quantificar o nível de segregação do sistema, utilizamos o parâmetro $\gamma_{1}$, introduzido por Belmonte el al 2008. Definido como a fração média de anéis vizinhos do tipo 2 em torno dos anéis do tipo 1
\begin{equation}
   \gamma_{1} = \left \langle \frac{n_{2}}{n_{1}+n_{2}} \right \rangle_{1},
\end{equation}
onde $\left \langle..\right \rangle_{1}$ é a média sobre todos os anéis do tipo 1 com $n_{1} + n_{2}$ vizinhos, sendo $n_{1}$ e $n_{2}$ o número de vizinhos do tipo 1 e 2 respectivamente.. O mesmo pode ser feito com o tipo 2, assim
\begin{equation}
   \gamma_{2} = \left \langle \frac{n_{1}}{n_{1}+n_{2}} \right \rangle_{2}.
\end{equation}

Em um sistema denso, o número de vizinhos $n_{1}+n_{2}$ é constante, assim
\begin{eqnarray}
       \label{energy_temporal2}
     \gamma_{1} &=&  \frac{1}{n_{1}+n_{2}} \left \langle n_{1} \right \rangle_{2} = \frac{1}{n_{1}+n_{2}}\frac{1}{N_{1}}{}\sum_{2}^{}n_{1} ,  \nonumber \\
    \label{energy_eq3}
      \gamma_{2} &=& \frac{1}{n_{1}+n_{2}} \left \langle n_{2} \right \rangle_{1} = \frac{1}{n_{1}+n_{2}}\frac{1}{N_{2}}{}\sum_{1}^{}n_{2},
     \end{eqnarray}
     sendo,
 \begin{eqnarray} 
    I = \sum_{2}^{}n_{1} = \sum_{1}^{}n_{2},
     \end{eqnarray}
      o tamanho da interfaces entre os diferentes tecidos celulares. Dessa forma, verificamos a seguinte relação
  \begin{eqnarray} 
    \frac{\gamma_{1}}{\gamma_{2}} = \frac{N_{2}}{N_{1}},
     \end{eqnarray}     
     que depende apenas da fração entre tipos celulares.
A fim de identificar as transições de tipo líquido para do tipo sólido utilizamos a função $\Delta(t)$ que quantifica as trocas de vizinhos entre os anéis,
\begin{eqnarray}
    \Delta(t) = \left \langle  \sum_{i}^{N} \frac{1}{n_{i}(t)}\sum_{k}^{n_{i}(t)}w_{i}(|\vec{R}_{i}(t)-\vec{R}_{k}(t)|)\right \rangle
\end{eqnarray}
onde,
\begin{eqnarray}
    w(r) = \left\{\begin{matrix}
1 & r  \leq  13 l_0, \\ 
 0& r>13l_0,
\end{matrix}\right.
\end{eqnarray}
onde $\left \langle ... \right \rangle$ representa média sobre realizações, $\vec{R}_{i}(t)$ a posição do centro de massa do anel $i$ no instante de tempo $t$, $n_{i}(t)$ o número de vizinhos do anel $i$ no instante de tempo $t$. A função $\Delta(t)$ quantifica o afastamento dos vizinhos de um dado anel $i$ no instante t ao longo do tempo. A sucessiva troca de vizinhos indica um estado configuracional tipo líquido, enquanto que a manutenção da vizinhança inicial sugere um estado tipo sólido. Definimos que o tempo característico de relaxação da função $\Delta(t)$ possui a seguinte relação: $\Delta(\tau_{\alpha}) = \frac{1}{e}$. O aumento de $\tau_{\alpha}$ indica menos trocas de vizinhos, sugerindo um estado do tipo sólido. Dessa forma, a partir $\tau_{\alpha}$ obtemos a informação do estado configuracional do agregado de anéis ativos.
% control parameters
%%%%%
\subsection{Parâmetros de Controle}
É útil identificar parâmetros adimensionais para controlar a dinâmica do sistema. No contexto do modelo de anéis \cite{PhysRevLett.121.248003} ou modelos de Voronoi \cite{PhysRevX.6.021011} existe um parâmetro adimensional chamado índice de forma, dado pela razão
\begin{eqnarray}
\label{p0}
p_{0} = \frac{P_{0}}{\sqrt{A_{0}}},
\end{eqnarray}
onde $P_{0} = nl_{P}$ é o perímetro de equilíbrio. $p_{0}$ mede a competição entre a tensão cortical celular e a contratilidade. Dessa forma, podemos definir $A_{0}$ como função de $p_{0}$, $l_{0}$ e $n$
\begin{equation}
    A_{0} = \left (\frac{nl_{0}}{p_{0}}  \right )^{2}
\end{equation}

Redimensionando os comprimentos em $l_{0}$, tempo em $\tau_{R}$ chegamos às equações adimensionais de movimento,
\begin{eqnarray}
\label{1}
  \frac{\mathrm{d}\vec{r}_{i,j}}{\mathrm{d} t}   &=& Pe\hat{n}_{j} -\vec{\nabla}_{i,j} E,  \\
\label{2}
\frac{\mathrm{d}\hat{n}_{j}}{\mathrm{d} t}  &=& G(\hat{n}_{j}\times \hat{v}^{j}_{cm})\times \hat{n}_{j}+ \sqrt{2}\vec{\xi}_{j}\times \hat{n}_{j},
\end{eqnarray}
onde $Pe$ é o número de Péclet rotacional~\cite{martin2018collective}, definido por
\begin{equation}
\label{7}
   Pe \equiv  \frac{v_0}{l_{0}D_{R}}.
\end{equation}

O parâmetro $Pe$ relaciona a difusão rotacional e o tempo de persistência do movimento. G é a intensidade de acoplamento adimensional ref[] entre $\hat{n}_{j}(t)$ e a velocidade do centro de massa do anel $\vec{v}^{j}_{cm}$, definida por
 \begin{equation}
     G = \frac{J}{D_{R}}.
 \end{equation}
Representa a taxa de alinhamento sobre a taxa de difusão angular, sendo expressa em termos do nível de ruído e do tempo de relaxamento típico $J^{-1}$ que os anéis levariam para se alinhar na ausência de ruído. Valores maiores de $G$ representam, portanto, uma tendência mais forte de alinhamento, ao considerar o efeito combinado das intensidades de alinhamento e do ruído. Podemos definir a taxa de empacotamento $\phi$ para o agregado de anéis que define a densidade do sistema. O valor da área de equilíbrio total $A_{total}$ é a soma da área de equílibrio imposta pela energia elástica $E_{A}$ mais a metade da área de cada disco que compõe o anel, assim
\begin{equation}
A_{total} = A0 + \frac{n\pi l_{0}^{2}}{8}.
\end{equation}
A taxa de empacotamento se relaciona com área através da seguinte relação
\begin{equation}
\phi = \frac{NA_{real}}{\pi R^{2}}.
\end{equation}
Portanto, $Pe$, $G$, $p_{0}$ e $\phi$ são os parâmetros adimensionais que controlam a dinâmica do sistema.
Os parâmetros fixados são: $\epsilon_{c}/\epsilon_{P} = 1$, $\epsilon_{A}/\epsilon_{P} = 50$, $\epsilon_{ext}/\epsilon_{P} = 75$, $n = 15$, $l_{0}= 1$ e $\sigma = l_{0} $. Integramos as equações de movimento, Eq.~\ref{1} e Eq.~\ref{2}, usando o método de Euler com um intervalo de tempo $\Delta t = 0,01$.
