\contentsline {chapter}{Capa}{i}{Doc-Start}%
\babel@toc {nil}{}\relax 
\contentsline {chapter}{Agradecimentos}{i}{chapter*.1}%
\contentsline {chapter}{\bf Conteúdo}{1}{chapter*.1}%
\contentsline {chapter}{\numberline {1}Introdução}{2}{chapter.1}%
\contentsline {chapter}{\numberline {2}Revisão Bibliográfica}{3}{chapter.2}%
\contentsline {section}{\numberline {2.1}Modelos Discretos para Dinâmica Celular}{3}{section.2.1}%
\contentsline {subsection}{\numberline {2.1.1}Partículas Brownianas Passivas}{4}{subsection.2.1.1}%
\contentsline {subsubsection}{Equação de Langevin: Processo de Ornstein-Uhlenbeck}{5}{section*.3}%
\contentsline {subsubsection}{Difusão de uma Partícula Browniana: Equação de Fürth }{7}{section*.5}%
\contentsline {subsubsection}{Difusão de um Sistema de Partículas Brownianas}{9}{section*.7}%
\contentsline {subsubsection}{Difusão Angular: Equação de Langevin Angular}{10}{section*.8}%
\contentsline {subsection}{\numberline {2.1.2}Partículas Brownianas Ativas (ABP)}{11}{subsection.2.1.2}%
\contentsline {subsubsection}{Difusão de uma Partícula Browniana Ativa}{16}{section*.11}%
\contentsline {subsubsection}{Difusão de um Sistema de Partículas Brownianas Ativas}{18}{section*.13}%
\contentsline {subsection}{\numberline {2.1.3}Modelos de Partícula Ativa com Alinhamento}{19}{subsection.2.1.3}%
\contentsline {subsubsection}{Modelo de Vicsek}{20}{section*.14}%
\contentsline {subsubsection}{Modelos com Auto-Alinhamento}{22}{section*.17}%
\contentsline {subsection}{\numberline {2.1.4}Modelo de Voronoi Ativo}{27}{subsection.2.1.4}%
\contentsline {subsection}{\numberline {2.1.5}Modelo de Anéis}{29}{subsection.2.1.5}%
\contentsline {subsection}{\numberline {2.1.6}Modelo de Anéis Ativos}{30}{subsection.2.1.6}%
\contentsline {chapter}{\numberline {3}Metodologia}{34}{chapter.3}%
\contentsline {section}{\numberline {3.1}Modelo de Anéis Ativos Modificado}{34}{section.3.1}%
\contentsline {subsection}{\numberline {3.1.1}Configuração do Sistema}{37}{subsection.3.1.1}%
\contentsline {subsection}{\numberline {3.1.2}Parâmetros de Ordem}{37}{subsection.3.1.2}%
\contentsline {subsection}{\numberline {3.1.3}Parâmetros de Controle}{38}{subsection.3.1.3}%
\contentsline {chapter}{\numberline {4}Resultados}{40}{chapter.4}%
\contentsline {chapter}{\numberline {5}Discussão, Conclusões e Perspectivas}{41}{chapter.5}%
